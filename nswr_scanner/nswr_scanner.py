import rclpy
from rclpy.node import Node
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs_py.point_cloud2 as pc2
from std_msgs.msg import Header
from std_msgs.msg import String
import math

class Scanner(Node):

    def __init__(self):
        super().__init__('nswr_scanner')

        # Subcribe data from the scanner
        self.subscription = self.create_subscription(PointCloud2,'/velodyne/velodyne_points',self.velodyne_callback,10)

        # Create topic for filtered data
        self.publisher = self.create_publisher(PointCloud2,'/velodyne_filtered', 10)

        # Fields in PointCloud2 ROS message for velodyne
        self.velodyne_fields = [
            PointField(name='x',
                       offset=0,
                       datatype=PointField.FLOAT32,
                       count=1),
            PointField(name='y',
                       offset=4,
                       datatype=PointField.FLOAT32,
                       count=1),
            PointField(name='z',
                       offset=8,
                       datatype=PointField.FLOAT32,
                       count=1),
        ]

    def velodyne_callback(self, msg):

        # Converting ROS message to point list
        points = pc2.read_points_list(msg,["x", "y", "z"])

        # Create new (filtered) point list without invalid points




        # Set frame_id to "velodyne" for VLP-16 and "laser" for MRS-6000;
        header = Header()
        header.frame_id = "velodyne"

        # Convert created (filtered) points ROS message
        # https://docs.ros.org/en/api/sensor_msgs/html/namespacesensor__msgs_1_1point__cloud2.html#a1dd7d701c1207e788076cf9e28ec3200
        # point_cloud_filtered = pc2.create_cloud()

        #  Publish message
        self.publisher.publish(point_cloud_filtered)


def main(args=None):

    rclpy.init(args=args)
    scanner = Scanner()

    rclpy.spin(scanner)

    scanner.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()